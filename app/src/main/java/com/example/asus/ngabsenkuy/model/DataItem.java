package com.example.asus.ngabsenkuy.model;

import com.google.gson.annotations.SerializedName;

public class DataItem {

    @SerializedName("nama_sekolah")
    private String namaSekolah;

    public void setNamaSekolah(String namaSekolah) {
        this.namaSekolah = namaSekolah;
    }

    public String getNamaSekolah() {
        return namaSekolah;
    }

}