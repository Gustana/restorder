package com.example.asus.ngabsenkuy.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseSekolah {

    @SerializedName("data")
    private List<DataItem> data;

    public void setData(List<DataItem> data) {
        this.data = data;
    }

    public List<DataItem> getListSekolah(){
        return data;
    }

    @Override
    public String toString() {
        return
                "ResponseSekolah{" +
                        "data = '" + data + '\'' +
                        "}";
    }
}